package zoo;

/**
 * Created by blackbear on 10/23/16.
 */
public class Lion extends Animal {
    public Lion(String nom, float poids) {
        super(nom, poids);
    }

    @Override
    public String crier() {
        return "Wouargh";
    }
}
