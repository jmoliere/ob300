package zoo;

/**
 * classe principale sans grand interet si ce n'est la methode main
 * c'est un programme executable en ligne de commandes.
 */
public class Main {

    public static void main(String[] args) {
        Zoo laPalmyre = new Zoo();
        boolean isOk = laPalmyre.getGardien().faireAppel();
        System.out.println("Appel est ok ? = " + isOk);
    }
}
