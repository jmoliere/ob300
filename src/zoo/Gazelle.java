package zoo;

/**
 * Created by blackbear on 10/23/16.
 */
public class Gazelle extends Animal {
    public Gazelle(String nom, float poids) {
        super(nom, poids);
    }

    @Override
    public String crier() {
        return "Gnop";
    }
}
