package zoo;

/**
 * Created by blackbear on 10/23/16.
 */
public class Zoo {
    private Animal[] mesAnimaux;

    private Gardien zooKeeper;

    public Zoo() {
        zooKeeper = new Gardien();
        mesAnimaux = new Animal[3];
        mesAnimaux[0] = new Lion("Simba", 350);
        mesAnimaux[1] = new Gazelle("Bambi", 65);
        mesAnimaux[2] = new Gazelle("Pimpa", 32);

        System.out.println("Zoo est OK");
    }

    public Animal[] getAnimaux() {
        return mesAnimaux;
    }

    public Gardien getGardien() {
        return zooKeeper;
    }
}
