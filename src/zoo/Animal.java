package zoo;

/**
 * Created by blackbear on 10/23/16.
 */
public abstract class Animal {
    protected String name;
    protected float weight;

    public Animal(String nom, float poids) {
        name = nom;
        weight = poids;
        System.out.println("J'ai creee un animal...");
    }

    /**
     * necessaire a rendre la classe abstraite
     * il n'y a pas de cri generique...
     *
     * @return
     */
    public abstract String crier();


    // toutes ces methodes sont interessantes - generees par IntelliJ
    public String getName() {
        return name;
    }

    public float getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Animal animal = (Animal) o;

        if (Float.compare(animal.weight, weight) != 0) return false;
        return name != null ? name.equals(animal.name) : animal.name == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (weight != +0.0f ? Float.floatToIntBits(weight) : 0);
        return result;
    }
}
